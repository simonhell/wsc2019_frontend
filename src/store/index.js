import Vue from 'vue'
import Vuex from 'vuex'
import {vuexPersist} from '../plugins/vuex-persist'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        //Contains all Circles/Slides
        /*
            Data-Structure:
            [{"id":1,"top":null,"bottom":4,"right":7,"left":6,"x":563,"y":122,"topCaption":"","leftCaption":"","rightCaption":"","bottomCaption":"","content":""}]
         */
        circles: [],
        //Contains all Links between circles.
        /*
            Data-Structure:
            [{"end":1,"directionEnd":"bottom","start":3,"directionStart":"top","x":638,"y":258,"id":4}]
         */
        links: [],
        circleIdCount: 0,
        linkIdCount: 0,
        activeDragCircleId: null,
        activeEditCircleId: null,
        activeActionsCircleId: null,
        //Gets set when dragging a link to create a new one
        /*
        {"start":2,"directionStart":"right","x":1028,"y":477}
         */
        activeLinkingStart: null,
        activeSlideId: null,
        activeLinkId: null,
        shiftKeyDown: false,
        canvasOffsetX: 0,
        canvasOffsetY: 0,
        canvasOffsetAtDragStart: null,
        canvasDraggingStartPosition: null,
        slideDirectionStart: null
    }
    ,
    mutations: {
        reset(store) {
            store.circles = [];
            store.links = [];
            store.activeDragCircleId = null;
            store.shiftKeyDown = false;
            store.activeEditCircleId = null;
            store.activeActionsCircleId = null;
            store.circleIdCount = 0;
            store.linkIdCount = 0;
            store.activeSlideId = null;
            store.selectedLinkId = null;
            store.activeLinkingStart = null;
            store.canvasOffsetX = 0;
            store.canvasOffsetY = 0;
            store.canvasOffsetAtDragStart = null;
            store.canvasDraggingStartPosition = null
        },
        setSelectedLinkId(store, id) {
            store.selectedLinkId = id;
        },
        startDraggingCanvas(store, position) {
            store.canvasDraggingStartPosition = position
            store.canvasOffsetAtDragStart = {x: store.canvasOffsetX, y: store.canvasOffsetY}
        },
        endDraggingCanvas(store) {
            store.canvasDraggingStartPosition = null;
            store.canvasOffsetAtDragStart = null;
        },
        moveCanvas(store, {x, y}) {
            store.canvasOffsetX = store.canvasOffsetAtDragStart.x + (x - store.canvasDraggingStartPosition.x);
            store.canvasOffsetY = store.canvasOffsetAtDragStart.y + (y - store.canvasDraggingStartPosition.y);
        },
        deleteLink(store, id) {
            const link = store.links.find(link => link.id === id);
            const start = store.circles.find(circle => circle.id === link.start);
            const end = store.circles.find(circle => circle.id === link.end);
            switch (id) {
                case start.top:
                    start.top = null;
                    break;
                case start.left:
                    start.left = null;
                    break;
                case start.right:
                    start.right = null;
                    break;
                case start.bottom:
                    start.bottom = null;
                    break;
            }
            switch (id) {
                case end.top:
                    end.top = null;
                    break;
                case end.left:
                    end.left = null;
                    break;
                case end.right:
                    end.right = null;
                    break;
                case end.bottom:
                    end.bottom = null;
                    break;
            }
            store.links = store.links.filter(link => link.id !== id);
        },
        setActiveActionsCircleId(store, id) {
            if (store.activeActionsCircleId === id) {
                store.activeActionsCircleId = null;
            } else {
                store.activeActionsCircleId = id;
            }
        },
        setEditCircleId(store, id) {
            store.activeEditCircleId = id;
        },
        saveActiveEditElement(store, element) {
            let circle = store.circles.find(circle => circle.id === store.activeEditCircleId);
            Object.assign(circle, element);
            store.activeEditCircleId = null;
        },
        addCircle(store, {x, y}) {
            store.circles.push({
                id: store.circleIdCount++,
                top: null,
                bottom: null,
                right: null,
                left: null,
                x: x || ((window.innerWidth - 100) / 2),
                y: y || ((window.innerHeight - 90) / 2),
                topCaption: "",
                leftCaption: "",
                rightCaption: "",
                bottomCaption: "",
                content: "",
            });
        },
        moveActiveCircle(store, mouseEvent) {
            const circleStart = store.circles.find(circle => circle.id === store.activeDragCircleId);
            circleStart.x = mouseEvent.clientX - 100 - store.canvasOffsetX;
            circleStart.y = mouseEvent.clientY - 90 - store.canvasOffsetY;
        },
        shiftKeyChange(store, boolean) {
            store.shiftKeyDown = boolean;
            if (!store.shiftKeyDown) {
                store.activeLinkingStart = null;
            }
        },
        setActiveDragCircle(store, id) {
            store.activeDragCircleId = id;
        },
        linkCircles(store, link) {
            const {directionStart, directionEnd} = link;
            link.id = store.linkIdCount++;
            store.links.push(link);
            const circleStart = store.circles.find(circle => circle.id === link.start);
            circleStart[directionStart] = link.id;
            const circleEnd = store.circles.find(circle => circle.id === link.end);
            circleEnd[directionEnd] = link.id;
        },
        setSlideDirection(store, direction) {
            store.slideDirectionStart = direction
        },
        setActiveSlideId(store, id) {
            store.activeSlideId = id;
        },
        setActiveLinkingStart(store, linkStart) {
            store.activeLinkingStart = linkStart;
        },
        moveActiveLinkingStart(store, mouseEvent) {
            store.activeLinkingStart.x = mouseEvent.clientX - 20;
            store.activeLinkingStart.y = mouseEvent.clientY - 20;
        },
        setLinks(store, links) {
            store.links = links;
        },
        setCircles(store, circles) {
            store.circles = circles;
        }
    },
    actions: {
        //Links to circle elements
        endLink({state, commit}, linkEnd) {
            if (linkEnd.end !== state.activeLinkingStart.start) {
                commit('linkCircles', {...linkEnd, ...state.activeLinkingStart});
                commit('setActiveLinkingStart', null);
            }
        },
        //Initializes the editor with one circle
        initializeEditor({state, commit}) {
            if (state.circles.length === 0) {
                commit('addCircle', {x: (window.innerWidth - 100) / 2, y: (window.innerHeight - 90) / 2});
            }
        },
        //Adds a new circle with a position based on the direction and position of the other circle
        addCircle({state, commit}, {circle, direction}) {
            if (!state.shiftKeyDown) {
                let x = circle.x;
                let y = circle.y;
                let directionEnd = direction;
                switch (direction) {
                    case 'top':
                        directionEnd = 'bottom';
                        y = circle.y - 250;
                        break;
                    case 'bottom':
                        directionEnd = 'top';
                        y = circle.y + 250;
                        break;
                    case 'right':
                        directionEnd = 'left';
                        x = circle.x + 250;

                        break;
                    case 'left':
                        directionEnd = 'right';
                        x = circle.x - 250;
                        break;
                    default:
                        break;
                }
                commit('addCircle', {x, y});
                commit('linkCircles', {
                    start: circle.id,
                    directionStart: direction,
                    end: state.circleIdCount - 1,
                    directionEnd: directionEnd
                })
            }
        },
        //Deletes the active Circle and all its links and the reference to the links in other circles
        deleteActiveCircle({state, commit}) {
            if (state.activeActionsCircleId !== null && state.circles.length > 1) {
                const id = state.activeActionsCircleId;
                if (state.activeSlideId === id) {
                    const newActiveSlide = state.links.find(slide => state.activeSlideId !== slide.id);
                    commit('setActiveSlideId', newActiveSlide.id);
                }
                commit('setLinks', state.links.filter(link => {
                    if (link.start !== id && link.end !== id) {
                        return true;
                    } else {
                        const circleStart = state.circles.find(circle => circle.id === link.start);
                        circleStart[link.directionStart] = null;
                        const circleEnd = state.circles.find(circle => circle.id === link.end);
                        circleEnd[link.directionEnd] = null;
                        return false;
                    }
                }));
                commit('setCircles',state.circles.filter(circle => circle.id !== id));
                commit('setActiveActionsCircleId', null);
            }
        }
    },
    plugins: [vuexPersist]
})
