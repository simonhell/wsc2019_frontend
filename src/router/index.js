import Vue from 'vue'
import VueRouter from 'vue-router'
import Editor from '../views/Editor';
import ViewPage from '../views/ViewPage';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Editor',
    component: Editor
  },
  {
    path: '/view',
    name: 'view',
    component: ViewPage
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
