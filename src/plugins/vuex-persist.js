export const vuexPersist = store => {
    const data = localStorage.getItem('store');
    if(data) {
        store.replaceState(JSON.parse(data));
    }
    store.subscribe((mutation, state) => {
        localStorage.setItem('store', JSON.stringify(state));
    })
}

export default {vuexPersist}